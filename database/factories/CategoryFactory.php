<?php

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->randomElement(['PHP', 'JAVASCRIPT', 'LARAVEL', 'JAVA', 'DISEÑO WEB', 'SERVIDORES', 'MYSQL', 'NSQL', 'VUEJS', 'ANGULAR', 'REACT', 'PYTHON']),
        'description' => $faker->sentence,
    ];
});
